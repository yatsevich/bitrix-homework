<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div>
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="review-block">
		<div class="review-text">
		
			<div class="review-block-title"><span class="review-block-name"><a href="/details.php"><?echo $arItem["PROPERTIES"]["AUTHOR"]["VALUE"]?></a></span><span class="review-block-description"><?echo $arItem["PROPERTIES"]["AUTHOR_POSITION"]["VALUE"]?></span></div>
			
			<div class="review-text-cont"><?echo $arItem["DETAIL_TEXT"];?></div>
		</div>
		<div class="review-img-wrap"><a href="/details.php"><img src="/bitrix/templates/.default/content/photo_1.jpg" alt="img"></a></div>
	</div>
	<?endforeach;?>
</div>