<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div>
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="review-block">
		<div class="review-text">
		
			<div class="review-block-title"><span class="review-block-name"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?echo $arItem["PROPERTIES"]["AUTHOR"]["VALUE"]?></a></span><span class="review-block-description"><?echo $arItem["PROPERTIES"]["AUTHOR_POSITION"]["VALUE"]?></span></div>
			
			<div class="review-text-cont"><?echo $arItem["DETAIL_TEXT"];?></div>
		</div>
		<div class="review-img-wrap"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="img"></a></div>
	</div>
	<?endforeach;?>
</div>