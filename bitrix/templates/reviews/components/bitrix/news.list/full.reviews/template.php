<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach($arResult["ITEMS"] as $arItem):?>
<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
<div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
	<div class="review-block">
		<div class="review-text">
		
			<div class="review-block-title"><span class="review-block-name"><a href="#"><?echo arItem["PRPERTY"]["AUTHOR"]["VALUE"];?></a></span><span class="review-block-description"><?echo arItem["PRPERTY"]["AUTHOR_POSITION"]["VALUE"]?></span></div>
			
			<div class="review-text-cont"><?echo arItem["DETAIL_TEXT"];?></div>
		</div>
		<div class="review-img-wrap"><a href="#"><img src="content/photo_1.jpg" alt="img"></a></div>
	</div>
</div>
<?endforeach;?>