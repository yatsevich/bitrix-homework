<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script>
	document.querySelector(".main_post > .main_title > h1").innerHTML = "<?echo $arResult['PROPERTIES']['AUTHOR_POSITION']['VALUE'];?>";
</script>

<div class="review-block">
	<div class="review-text">
		<div class="review-text-cont">
			<?echo $arResult["DETAIL_TEXT"];?>
		</div>
		<div style="float: right; font-style: italic;">
			<?echo $arResult["PROPERTIES"]["AUTHOR"]["VALUE"];?>
		</div>
	</div>
	<div style="clear: both;" class="review-img-wrap"><img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="img"></div>
</div>
<a href="/reviews/" class="ps_backnewslist"> &larr; К списку отзывов</a>
