<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="rw_reviewed">
	<div class="rw_slider">
		<h4>Отзывы</h4>
		<ul id="foo">
			<?foreach($arResult["ITEMS"] as $arItem):?>
			<li>
				<div class="rw_message">
					<img src="/bitrix/templates/.default/content/8.png" class="rw_avatar" alt=""/>
					<span class="rw_name">Сергей Антонов</span>
					<span class="rw_job">Руководитель финансового отдела “Банк+”</span>
					<p>“Покупал офисные стулья и столы, остался очень доволен! Низкие цены, быстрая доставка, обслуживание на высоте! Спасибо!”</p>
					<div class="clearboth"></div>
					<div class="rw_arrow"></div>
				</div>
			</li>
			<?endforeach;?>
		</ul>
		<div id="rwprev"></div>
		<div id="rwnext"></div>
		<a href="" class="rw_allreviewed">Все отзывы</a>
	</div>
</div>

<!--div class="sl_slider" id="slides">
	<div class="slides_container">
		<?foreach($arResult["ITEMS"] as $arItem):?>
		<div>
			<div>
				<?if(is_array($arItem["PREVIEW_PICTURE"])):?>
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" />
				<?endif;?>
				<h2><a href="<?=$arItem["PROPERTIES"]['LINK']['VALUE']?>"><?echo $arItem["NAME"]?></a></h2>
				<p><?echo $arItem["PREVIEW_TEXT"];?></p>
				<a href="<?=$arItem["PROPERTIES"]['LINK']['VALUE']?>" class="sl_more">Подробнее &rarr;</a>
			</div>
		</div>
		<?endforeach;?>
	</div>
</div-->

