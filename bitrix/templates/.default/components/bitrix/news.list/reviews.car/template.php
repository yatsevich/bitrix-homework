<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<script type="text/javascript" >
	$(document).ready(function(){
	
		$("#foo").carouFredSel({
			items:4,
			prev:'#rwprev',
			next:'#rwnext',
			scroll:{
				items:1,
				duration:2000
			}
		});	
	});	
</script>

<div class="rw_reviewed">
	<div class="rw_slider">
		<h4>Отзывы</h4>
		<ul id="foo">
			<?foreach($arResult["ITEMS"] as $arItem):?>
			<li>
				<div class="rw_message">
					<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arItem["PREVIEW_PICTURE"])):?>
						<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
							<a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img
									class="rw_avatar"
									border="0"
									src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
									width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
									height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
									alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
									title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
									style="float:left"
									/></a>
						<?else:?>
							<img
								class="rw_avatar"
								border="0"
								src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
								width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
								height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
								alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
								title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
								style="float:left"
								/>
						<?endif;?>
					<?endif?>
					<span class="rw_name"><?echo $arItem["PROPERTIES"]["AUTHOR"]["VALUE"]?></span>
					<span class="rw_job"><?echo $arItem["PROPERTIES"]["AUTHOR_POSITION"]["VALUE"]?></span>
					<p><?echo $arItem["DETAIL_TEXT"]?></p>
					<div class="clearboth"></div>
					<div class="rw_arrow"></div>
				</div>
			</li>
			<?endforeach;?>
		</ul>
		<div id="rwprev"></div>
		<div id="rwnext"></div>
		<a href="/reviews/" class="rw_allreviewed">Все отзывы</a>
	</div>
</div>